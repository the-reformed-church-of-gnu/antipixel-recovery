https://cyber.dabamos.de/88x31/index3.html

```js
function downloadImage(url, file) {
  fetch(url)
  .then(response => response.blob())
  .then(blob => {
		const link = document.createElement('a');
		link.href = URL.createObjectURL(blob);
		link.download = file;
		link.click();
  })
	.catch(console.error)
}

[...document.getElementsByTagName('img')].forEach((elem) => {
// 	console.log(elem.src)
const fileName = elem.src.split('/').reverse()[0]
downloadImage(elem.src, fileName)

})
```
